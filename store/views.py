from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.http import HttpResponse, Http404, JsonResponse
from .models import Item, Order, OrderItem
from .forms import ItemModelForm, UserLoginForm, UserRegisterForm, OrderCreateForm, OrderEditForm
from django.contrib import messages
from django.urls import reverse_lazy
from django_tables2 import RequestConfig
from django.views.generic import (
    ListView,
    CreateView,
    DeleteView,
    UpdateView,
    DetailView
)
from .filters import Item_Filter
import json

from django.contrib.auth import (
        authenticate,
        get_user_model,
        login,
        logout,
)
from .tables import ItemTable, OrderItemTable
from django.template.loader import render_to_string


class CreateOrderView(CreateView):
    template_name = 'pos/form.html'
    form_class = OrderCreateForm
    model = Order

    def get_success_url(self):
        self.new_object.refresh_from_db()
        return reverse('update_order', kwargs={'pk': self.new_object.id})

    def form_valid(self, form):
        object = form.save()
        object.refresh_from_db()
        self.new_object = object
        return super().form_valid(form)

class OrderUpdateView(UpdateView):
    model = Order
    template_name = 'pos/order_update.html'
    form_class = OrderEditForm

    def get_success_url(self):
        return reverse('update_order', kwargs={'pk': self.object.id})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        instance = self.object
        qs_p = Item.objects.filter()[:12]
        items = ItemTable(qs_p)
        order_items = OrderItemTable(instance.order_items.all())
        RequestConfig(self.request).configure(items)
        RequestConfig(self.request).configure(order_items)
        context.update(locals())
        return context

def login_view(request):
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect('/')
    context = {
        'form' : form,
    }
    return render(request, 'login.html', context)

def register_view(request):
    next = request.GET.get('next')
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password')
        user.set_password(password)
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        if next:
            return redirect(next)
        return redirect('/')
    context = {
        'form' : form,
    }
    return render(request, 'signup.html', context)

def logout_view(request):
    logout(request)
    return redirect('/login')


class ItemCreateView(LoginRequiredMixin, CreateView):
    template_name = 'items/item_create.html'
    form_class = ItemModelForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class ItemListView(LoginRequiredMixin, ListView):
    model = Item
    template_name = 'items/item_index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = Item_Filter(self.request.GET, queryset=self.get_queryset())
        return context

class ItemDetailView(LoginRequiredMixin, DetailView):
    template_name = 'items/item_detail.html'
    queryset = Item.objects.all()

    def get_object(self):
        id = self.kwargs.get('id')
        return get_object_or_404(Item, id=id)

class ItemUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'items/item_create.html'
    form_class = ItemModelForm

    def get_object(self):
        id = self.kwargs.get('id')
        return get_object_or_404(Item, id=id)

#AJAX

def ajax_search_products(request, pk):
    instance = get_object_or_404(Order, id=pk)
    q = request.GET.get('q', None)
    items = Item.objects.filter(item_name__startswith=q) if q else Item.objects.all()
    items = items[:12]
    items = ItemTable(items)
    RequestConfig(request).configure(items)
    data = dict()
    data['items'] = render_to_string(template_name='include/product_container.html',
                                        request=request,
                                        context={
                                            'items': items,
                                            'instance': instance
                                        })
    return JsonResponse(data)

def ajax_add_product(request, pk, dk):
    instance = get_object_or_404(Order, id=pk)
    item = get_object_or_404(Item, id=dk)
    order_item, created = OrderItem.objects.get_or_create(order=instance, item=item)
    if created:
        order_item.quantity = 1
        order_item.price = item.item_price
    else:
        order_item.quantity += 1
    order_item.save()
    instance.refresh_from_db()
    order_items = OrderItemTable(instance.order_items.all())
    RequestConfig(request).configure(order_items)
    data = dict()
    data['result'] = render_to_string(template_name='include/order_container.html',
                                      request=request,
                                      context={'instance': instance,
                                               'order_items': order_items
                                               }
                                    )
    return JsonResponse(data)

def ajax_modify_order_item(request, pk, action):
    order_item = get_object_or_404(OrderItem, id=pk)
    item = order_item.item
    instance = order_item.order
    if action == 'remove':
        order_item.quantity -= 1
        if order_item.quantity < 1: order_item.quantity = 1
    if action == 'add':
        order_item.quantity += 1
    order_item.save()
    if action == 'delete':
        order_item.delete()
    data = dict()
    instance.refresh_from_db()
    order_items = OrderItemTable(instance.order_items.all())
    RequestConfig(request).configure(order_items)
    data['result'] = render_to_string(template_name='include/order_container.html',
                                      request=request,
                                      context={
                                          'instance': instance,
                                          'order_items': order_items
                                      }
                                      )
    return JsonResponse(data)





# def item_create(request):
#     form = item_creation()
#     if request.method == 'POST':
#         form = item_creation(request.POST)
#         if form.is_valid():
#             # item_price = form.cleaned_data['item_price']
#             # form.instance.item_margin = item_price
#             Item.objects.create(**form.cleaned_data)
#             # form.save()
#             form = item_creation()
#     context = {
#         'form': form
#     }
#     return render(request, 'items/item_create.html', context)




# def item_detail(request, item_id):
#     try:
#         items = Item.objects.get(pk=item_id)
#     except Item.DoesNotExist:
#         raise Http404("Item does not exist")
#     return render(request, 'items/item_detail.html', {'items': items})

