import django_filters
from django import forms

from .models import Item

class Item_Filter(django_filters.FilterSet):

    # item_name = django_filters.CharFilter(lookup_expr='icontains')

    item_name = django_filters.CharFilter(label='', lookup_expr='icontains', widget=forms.TextInput(attrs={
            'placeholder': 'Search place', 'class': 'form-control', 'onClick':'this.select()'}))
    
    class Meta:
        model = Item
        fields = ['item_name']