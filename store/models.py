from django.db import models
from django.urls import reverse
from django.db.models import Sum
from djmoney.models.fields import MoneyField
from django.contrib.auth.models import User
from django.db.models.signals import post_delete
from django.dispatch import receiver

import datetime
from decimal import Decimal

class OrderManager(models.Manager):

    def active(self):
        return self.filter(active=True)


class Item(models.Model):
    creation = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, editable=False, null=True, blank=True, on_delete=models.CASCADE)
    item_name = models.CharField(max_length=100, unique=True)
    item_status = models.BooleanField(default=True)
    item_price = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    item_cost = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    item_margin = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    item_description = models.TextField()


    def __str__(self):
        return '%s' % (self.item_name)


    def get_absolute_url(self):
        return reverse("store:item-index")


class Order(models.Model):
    date = models.DateTimeField(default=datetime.datetime.now())
    creation = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    title = models.CharField(blank=True, max_length=150)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    is_paid = models.BooleanField(default=False)

    class Meta:
        ordering = ['-date']

    def save(self, *args, **kwargs):
        order_items = self.order_items.all()
        self.total_price = order_items.aggregate(Sum('total_price'))['total_price__sum'] if order_items.exists() else 0.00
        print(self.total_price)
        super().save(*args, **kwargs)


    def __str__(self):
        return 'Order {0}'.format(self.id)

class OrderItem(models.Model):
    
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='order_items')
    quantity = models.PositiveIntegerField(default=0)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, verbose_name="Item Total")

    def save(self, *args, **kwargs):
        self.total_price = Decimal(self.quantity) * Decimal(self.price)
        super().save(*args, **kwargs)
        self.order.save()

@receiver(post_delete, sender=OrderItem)
def delete_order_item(sender, instance, **kwargs):
    item = instance.item
    item.save()
    instance.order.save()




# Create your models here.
