function get_item_price(){
    var profit = (id_item_cost.value * (id_item_margin.value/100));
    var item_price = parseFloat(profit) + parseFloat(id_item_cost.value);
    return id_item_price.value = item_price.toFixed(2);

}
function get_item_margin(){
    var gross_profit = (id_item_price.value - id_item_cost.value)
    console.log(gross_profit)
    var margin = (gross_profit / id_item_cost.value) * 100
    return id_item_margin.value = margin.toFixed(2);
}

function remove_string(){
    var price_number = parseFloat(id_item_price.value.replace(/₱/, ""));
    var cost_number = parseFloat(id_item_cost.value.replace(/₱/, ""));
    id_item_cost.value = cost_number;
    id_item_price.value = price_number;
    return id_item_price.value, id_item_cost.value
}