from django.urls import path, include
from . import views
from .views import (ItemListView, ItemDetailView, ItemCreateView, ItemUpdateView,
                    CreateOrderView, OrderUpdateView, ajax_search_products, ajax_add_product, ajax_modify_order_item)
from .filters import Item_Filter

app_name = 'store'
urlpatterns = [
    path('items/', views.ItemListView.as_view(), name='item-index'),
    path('create/', views.ItemCreateView.as_view(), name='item-create'),
    path('detail/<int:id>/', views.ItemDetailView.as_view(), name='item-detail'),
    path('update/<int:id>/', views.ItemUpdateView.as_view(), name='item-create'),
    path('pos/', CreateOrderView.as_view(), name='create-order'),
    path('transaction/<int:pk>/', OrderUpdateView.as_view(), name='update_order'),

    #ajax add item in order
    path('ajax/search-products/<int:pk>/', ajax_search_products, name='ajax-search'),
    path('ajax/add-product/<int:pk>/<int:dk>/', ajax_add_product, name='ajax-add'),
    path('ajax/modify-product/<int:pk>/<slug:action>', ajax_modify_order_item, name='ajax_modify'),
]