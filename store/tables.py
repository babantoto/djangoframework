import django_tables2 as tables

from .models import OrderItem, Order, Item

class OrderTable(tables.Table):
    action = tables.TemplateColumn(
        '<a href="{{ record.get_edit_url }}" class="btn btn-info"><i class="fa fa-edit"></i></a>',)

    class Meta:
        model = Order
        template_name = 'django_tables2/bootstrap.html'
        fields = ['date', 'title']

class ItemTable(tables.Table):
    action = tables.TemplateColumn(
        '<button class="btn btn-info add_button" data-href="{% url "store:ajax-add" instance.id record.id%}">Add!</a>',
    )
    
    class Meta:
        model = Item
        template_name = 'django_tables2/bootstrap.html'
        fields = ['item_name', 'item_price']

class OrderItemTable(tables.Table):
    action = tables.TemplateColumn('''
            <button data-href="{% url "store:ajax_modify" record.id "add" %}" class="btn btn-success edit_button"><i class="fa fa-arrow-up"></i></button>
            <button data-href="{% url "store:ajax_modify" record.id "remove" %}" class="btn btn-warning edit_button"><i class="fa fa-arrow-down"></i></button>
            <button data-href="{% url "store:ajax_modify" record.id "delete" %}" class="btn btn-danger edit_button"><i class="fa fa-trash"></i></button>
    ''')

    class Meta:
        model = OrderItem
        template_name = 'django_tables2/bootstrap.html'
        fields = ['item', 'quantity', 'total_price']
