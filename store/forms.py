from django import forms
from .models import Item, Order, OrderItem
from django.contrib.auth import (
        authenticate,
        get_user_model
)

User = get_user_model()

class BaseForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

class OrderCreateForm(BaseForm, forms.ModelForm):
    date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))

    class Meta:
        model = Order
        fields = ['date', 'title' ]

class OrderEditForm(BaseForm, forms.ModelForm):

    class Meta:
        model = Order
        fields = ['date', 'title', 'is_paid']

class UserLoginForm(forms.Form):
        username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
        password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

        def clean(self, *args, **kwargs):
                username = self.cleaned_data.get('username')
                password = self.cleaned_data.get('password')
                
                if username and password:
                        user = authenticate(username=username, password=password)
                        if not user:
                                raise forms.ValidationError('This user does not exist')
                        if not user.check_password(password):
                                raise forms.ValidationError('Incorrect password')
                        if not user.is_active:
                                raise forms.ValidationError('This user is not active')
                return super(UserLoginForm, self).clean(*args, **kwargs)

class UserRegisterForm(forms.ModelForm):
        username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
        email = forms.EmailField(label='Email Address', widget=forms.TextInput(attrs={'class': 'form-control'}))
        email2 = forms.EmailField(label='Confirm Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
        password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
        
        class Meta:
                model = User
                fields = [
                        'username',
                        'email',
                        'email2',
                        'password'
                ]
        
        def clean(self, *args, **kwargs):
                email = self.cleaned_data.get('email')
                email2 = self.cleaned_data.get('email2')
                if email != email2:
                        raise forms.ValidationError('Emails must match')
                email_qs = User.objects.filter(email=email)
                if email_qs.exists():
                        raise forms.ValidationError('Email already exists')
                return super(UserRegisterForm, self).clean(*args, **kwargs)
                


class ItemModelForm(forms.ModelForm):
        item_name = forms.CharField(label='Item Name', required=True, widget=forms.TextInput(attrs={  
                                                                                                'class': 'form-control',
                                                                                                'placeholder': 'Item Name',
                                                                                                'onClick': 'this.select()'                                                             
        }))
        item_status = forms.CheckboxInput()
        item_price = forms.DecimalField(label='Item Price', min_value=0.00, initial=0.00, widget=forms.TextInput(attrs={
                                                                                                'class':'form-control',
                                                                                                'oninput':'get_item_margin()',
                                                                                                'onClick': 'this.select()'

        }))
        item_margin = forms.DecimalField(label='Item Margin', min_value=0.00, initial=0.00, widget=forms.TextInput(attrs={
                                                                                                'class':'form-control',
                                                                                                'oninput':'get_item_price()',
                                                                                                'onClick': 'this.select()'
        }))
        item_cost = forms.DecimalField(label='Item Cost', min_value=0.00, initial=0.00, widget=forms.TextInput(attrs={
                                                                                                'class':'form-control',
                                                                                                'onClick': 'this.select()'
        }))
        item_description = forms.CharField(label='Description', required=False, widget=forms.Textarea(attrs={
                                                                                                'placeholder': 'Item Description',
                                                                                                'rows': 10,
                                                                                                'cols': 100,
        }))
        class Meta:
                model = Item
                fields = [
                        'item_name',
                        'item_cost',
                        'item_margin',
                        'item_price',
                        'item_status',
                        'item_description',
                ]


