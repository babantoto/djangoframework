from django.apps import AppConfig


class FirestationsystemConfig(AppConfig):
    name = 'firestationsystem'
