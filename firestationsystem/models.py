from django.db import models
import datetime

# Create your models here.
class Building(models.Model):
    creation_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    building_name = models.CharField(max_length=100)
    building_address = models.CharField(max_length=100)
    building_stories = models.IntegerField(default=0)
    building_description = models.TextField(blank=True)
