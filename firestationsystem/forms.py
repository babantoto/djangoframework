from django import forms

from .models import Building

class create_building(forms.ModelForm):
    class Meta:
        model = Building
        fields = [
            'building_name',
            'building_address',
            'building_stories',
            'building_description',
        ]

class create_building_form(forms.Form):
    building_name = forms.CharField(label='Building', widget=forms.TextInput(attrs={'placeholder':'Building Name'}))
    building_address = forms.CharField(label='Address', widget=forms.TextInput(attrs={'placeholder':'Building Address'}))
    building_stories = forms.IntegerField(label='Stories', initial=1, min_value=1)
    building_description = forms.CharField(label='', required=False, widget=forms.Textarea(attrs={  'class':'class-text-area',
                                                                                                    'placeholder':'Description',
                                                                                                    'rows': 20,
                                                                                                    'cols': 120}))
