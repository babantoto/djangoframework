from django.urls import path
from . import views

app_name = 'firestationsystem'
urlpatterns = [
    path('', views.building_index, name='building_index'),
    path('create/', views.building_create_view, name='building_create_view'),
    path('<int:building_id>', views.building_detail, name='building_detail')
]