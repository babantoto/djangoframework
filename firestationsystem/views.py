from django.shortcuts import render
from django.http import HttpResponse
from .models import Building
from .forms import create_building, create_building_form

# Create your views here.
def building_index(request):
    building_list = Building.objects.all()
    return render(request, 'building_registration/building_index.html', {'building_list': building_list})

def building_create_view(request):
    form = create_building_form()
    if request.method == 'POST':
        form = create_building_form(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            Building.objects.create(**form.cleaned_data)
            form = create_building_form()
    context = {
       'form' : form
    }
    return render(request, 'building_registration/create_building_record.html', context)

def building_detail(request, building_id):
    return HttpResponse("building details here")